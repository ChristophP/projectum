jQuery.sap.declare("projectum.Component");

// load dependencies
jQuery.sap.require("projectum.Router");
jQuery.sap.require('projectum.util.Formatter');

sap.ui.core.UIComponent.extend("projectum.Component", {
	metadata : {
        name : "Room Manager",
        version : "1.0",
        includes : [],
        dependencies : {
            libs : ["sap.m", "sap.ui.layout"],
            components : []
        },
    	rootView : {
    		viewName: "projectum.view.app",
    		id: "appView",
    		type: 'XML',
    		viewData: {
    			component: this
    		}
    	},
    	config : {
            resourceBundle : "i18n/messageBundle.properties",
            serviceConfig : {
                name : "ABAP-OData-Service",
                serviceUrl : "/odata-projectum.svc"
            }
        },
        routing : {
			config : {
				routerClass : projectum.Router,
				viewType : "XML",
				viewPath : "projectum.view",
				targetAggregation : "content",
				clearTarget : true
			},
			routes : [
			    // main
				{
					pattern : "",
					name : "main",
					view : "projectOverview",
					targetControl: "appView--tabPane--tab1",
					subroutes : [
									{
										pattern : "project/{id}",
										name : "projectDetail",
										view : "projectDetailTeamMember"
									},
									{
										pattern : "project-manager/{id}",
										name : "projectDetailManager",
										view : "projectDetailManager"
									},
									
									
								]
						
				},
				// timesheet
				{
					pattern : "timesheet",
					name : "timesheet",
					view : "timeSheet",
					targetControl: "appView--tabPane--tab2",
					
				},
				// login
				{
					pattern : "login",
					name : "login",
					view : "login",
					targetControl: "appView--tabPane--tab1"
				},
				// catchall
				{
					pattern: "",
					name : "catchall",
					view : "projectOverview",
					targetControl: "appView--tabPane--tab1",
					pattern : ":all*:"
				}
			]
		}
    },
    
    init : function() {

        sap.ui.core.UIComponent.prototype.init.apply(this, arguments);

        
        var mConfig = this.getMetadata().getConfig();

        // always use absolute paths relative to our own component
        // (relative paths will fail if running in the Fiori Launchpad)
        var rootPath = jQuery.sap.getModulePath("projectum");

        // set i18n model
        var i18nModel = new sap.ui.model.resource.ResourceModel({
            bundleUrl : [rootPath, mConfig.resourceBundle].join("/")
        });
        this.setModel(i18nModel, "i18n");
        
        // Create and set domain model to the component
        // get url depending on environment
		function buildServiceUrl(sServiceUrl) {
			if(location.hostname == 'localhost') {
				sServiceUrl = 'proxy' + sServiceUrl;
			}
			return sServiceUrl;
		}
        
        var sServiceUrl = buildServiceUrl( mConfig.serviceConfig.serviceUrl );
        var oModel = new sap.ui.model.odata.ODataModel(sServiceUrl, true);
        this.setModel(oModel);

        // set device model
        var deviceModel = new sap.ui.model.json.JSONModel({
            isTouch : sap.ui.Device.support.touch,
            isNoTouch : !sap.ui.Device.support.touch,
            isPhone : sap.ui.Device.system.phone,
            isNoPhone : !sap.ui.Device.system.phone,
            listMode : 'SingleSelectMaster', // sap.ui.Device.system.phone ? "None" : "SingleSelectMaster",
            listItemType : 'Active' // sap.ui.Device.system.phone ? "Active" : "Inactive"
        });
        deviceModel.setDefaultBindingMode("OneWay");
        this.setModel(deviceModel, "device");
        
        var oRouter = this.getRouter();
        oRouter.initialize();
		
        
            
    }
    
});