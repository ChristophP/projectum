jQuery.sap.declare("projectum.util.Formatter");

projectum.util.Formatter = {

	uppercaseFirstChar : function(sStr) {
		return sStr.charAt(0).toUpperCase() + sStr.slice(1);
	},

	discontinuedStatusState : function(sDate) {
		return sDate ? "Error" : "None";
	},

	discontinuedStatusValue : function(sDate) {
		return sDate ? "Discontinued" : "";
	},

	currencyValue : function (value) {
		return parseFloat(value).toFixed(2);
	},
	
	sprintf : function(sFormat) {
		var args = arguments,
	    string = args[0],
	    i = 1;
	    return string.replace(/%((%)|s|d)/g, function (m) {
	        // m is the matched format, e.g. %s, %d
	        var val = null;
	        if (m[2]) {
	            val = m[2];
	        } else {
	            val = args[i];
	            // A switch statement so that the formatter can be extended. Default is %s
	            switch (m) {
	                case '%d':
	                    val = parseFloat(val);
	                    if (isNaN(val)) {
	                        val = 0;
	                    }
	                    break;
	            }
	            i++;
	        }
	        return val;
	    });
	},
	
	dateToBool: function(oDate) {
		var iTime = oDate.getTime();
		return iTime > 0;
	},
	
	dateToString: function(oDate) {
		// need to check in case formatter is called before model is loaded(with null value)
		if(oDate instanceof Date) {
			return oDate.toDateString();
		}
	},
	
	dateToWeek: function(oDate) {	
	    var onejan = new Date(oDate.getFullYear(),0,1);
	    return Math.ceil((((oDate - onejan) / 86400000) + onejan.getDay()+1)/7);
	},
	
	getIdFromPath: function(sPath) {
		var match = sPath.match(/\((.*?)\)/);
		return match ? match[1] : null;
	},
	
	idToStatusIcon: function(iId) {
		return app.statusInfo[iId].icon;
	},
	
	idToStatusColor: function(iId) {
		return app.statusInfo[iId].color;
	},
	
	idToStatusText: function(iId) {
		return app.statusInfo[iId].text;
	},
	
	getLevelByManagerId: function(iManagerId) {
		if(iManagerId == app.session.getUserId()) {
			return 'Manager';
		} else {
			return 'Team member';
		};
	}

};