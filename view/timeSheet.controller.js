jQuery.sap.require('projectum.util.Formatter');

sap.ui.controller("projectum.view.timeSheet", {
	
	_currentWeek: null,
	
	_saveButton: null,
	
	_saveDialog: null,
	
	
	
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf zyss15t4projectum.app
*/
	onInit: function() {
		
		var oView = this.getView(),
			sFormat = '/Employees(%d)';
	
		if( app.session.isLoggedIn() ) {
			oView.bindElement( projectum.util.Formatter.sprintf(sFormat, app.session.getUserId() ) );
		}
		
		this.initCalendar();
		
		

		// set element bindings for important controls
		sap.ui.core.UIComponent.getRouterFor(this).attachRoutePatternMatched(function(oEvent) {
			// when detail navigation occurs,
			// update the binding context
			if (oEvent.getParameter('name') === 'timesheet') {
				// insert action repeated every time the page loads
			}
		}, this);
		
		oView.attachEventOnce('beforeRendering', function(oEvent) {
			var oList = oView.byId('project-panels');
		
			oList.getBinding('content').attachEventOnce('dataReceived', function(oEvent) {
				// expand first panel
				var aContent = oList.getContent();
				if(aContent.length) {
					aContent[0].setExpanded(true);
				}

			}, this);
			
			// delay till all relevant controls are loaded
			oList.getBinding('content').attachEvent('dataReceived', function(oEvent) {
				var oContent = oList.getContent();
				
				if(oContent.length) {
					oContent[0].getBinding('content').attachEventOnce('dataReceived', function() {
						this.updateStatus();
					}, this);
				}
			}, this);
			
		}, this);
		
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf zyss15t4projectum.app
*/
	onBeforeRendering: function() {
		
	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf zyss15t4projectum.app
*/
	onAfterRendering: function() {
		
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf zyss15t4projectum.app
*/
//	onExit: function() {
//
//	},
	
	updateStatus: function() {
		var oModel = this.getOwnerComponent().getModel(),
			oPanels = this.getView().byId('project-panels').getContent(),
			self = this;
		
		// for all panels
		oPanels.forEach(function(oPanel) {
			var oContent = oPanel.getContent(),
				iTaskId, sPath;
			// get first tasks
			if(oContent.length) {
				iTaskId = oContent[0].data('task');
				
				sPath =  projectum.util.Formatter.sprintf('/History(TaskId=%d,HisCw=%d)', iTaskId, self._currentWeek);
				
				// query history table
				oModel.read(sPath, {
					success: function(oResponse) {
						// set corresponding symbol
						self.setStatusSymbol(oResponse.HisStatus, oPanel);
					},
					error: function(oResponse) {
						
						if(oResponse.statusCode == 404) {
							// put save button
							self.setStatusSymbol(null, oPanel);
						} else {
							console.log('Some unexpected error occured');
						}
						
					}
				});
				
				
			}
			
			
		});
		
		
		
	},
	
	setStatusSymbol: function(iStatus, oControl) {
		var oToolbar = oControl.getHeaderToolbar(),
			iContentLength = oToolbar.getContent().length,
			mStatusInfo, oInsertControl;
		
		if(iStatus != null) {
			mStatusInfo = app.statusInfo[iStatus];
			
			oInsertControl = new sap.ui.core.Icon({ 
				src: mStatusInfo.icon, 
				color :  mStatusInfo.color,
				tooltip: mStatusInfo.text,
				size: '36px',
				width: '3em'
			});
			
		} else {
			oInsertControl = new sap.m.Button({
				press: jQuery.proxy(this.onSaveButtonPress, this),
				icon: 'sap-icon://save',
				text: 'Save'
			});
		}
		
		oToolbar.removeContent(iContentLength - 1);
		oToolbar.insertContent( oInsertControl, iContentLength);
		
		/*
		// check status and show corresponding symbol
		switch (iStatus) {
			case app.timeStatus.PENDING:
				break;
			case app.timeStatus.APPROVED:
				break;
			case app.timeStatus.REJECTED:
				break;
		}
		*/
	},
	
	initCalendar: function() {
		
		var oView = this.getView();
		
		// this will point to the calendar
		function fnUpdateWeek() {
			var dWeek = new Date( oView.byId('durationCalendar').getCurrentDate() ),
				iWeek = projectum.util.Formatter.dateToWeek( dWeek );
			
			this._currentWeek = iWeek;
			oView.byId('weekHeader').setNumber( projectum.util.Formatter.sprintf( 'Week %d', iWeek ) );
			
		};
		
		oView.byId('durationCalendar').attachChangeCurrentDate(fnUpdateWeek, this)
			.fireChangeCurrentDate()
			.attachChangeCurrentDate(this.updateStatus, this);
		
		
	},
	
	/**
	 * save history on press of save button
	 */
	saveHours: function(oButton) {
		var oView = this.getView(),
			oModel = this.getOwnerComponent().getModel(),
			oToolbar, oPanel, iWeek;
		
		iWeek = projectum.util.Formatter.dateToWeek( new Date( oView.byId('durationCalendar').getCurrentDate() ) );
		
		oButton.setEnabled(false);
		
		oToolbar = oButton.getParent();
		
		// close Panel
		oPanel = oToolbar.getParent();
		
		//oModel.setUseBatch(true);
		
		// find all times for tasks
		oPanel.getContent().forEach(function(oForm) {
			var oContent = oForm.getContent(), 
				oPayload;
		
			oPayload = {
				TaskId: +oForm.data('task'),
				HisCw: iWeek,
				HisWorkload: new sap.ui.model.odata.type.Decimal({ decimals: 2 }).formatValue( oContent[1].getValue(), 'string' ),
				HisComment: oContent[2].getValue(),
				HisStatus: app.timeStatus.PENDING
			};
			
			// disable batch mode(does not work with default implementation)
			oModel.setUseBatch(false);
			
			oModel.create('/History', oPayload, {
				success: function() {
					console.log('Created');
				},
				error: function() {
					console.log('Could not be created');
				}
			});
			
			oModel.setUseBatch(true);
			// enable batch mode
			
			
		});
		
		oPanel.setExpanded(false);
		this.setStatusSymbol(app.timeStatus.PENDING, oPanel);
		
		// show message
		sap.m.MessageToast.show('Times successfully submitted. Awaiting approval by manager.');
	
	},
	
	onWorkhourInputChange: function(oEvent) {
		var oSource = oEvent.getSource(),
			oType = new sap.ui.model.odata.type.Decimal({ decimals: 2 }, { nullable: false, precision: 5, scale: 2 });
		
		// throws exception when valid is not valid
		try {
			oType.validateValue(oSource.getValue());
			oSource.setValueState(sap.ui.core.ValueState.None);
		} catch(e) {
			oSource.setValueState(sap.ui.core.ValueState.Error);
		}
		
	},
	
	getSaveDialog: function() {
		
		if(!this._saveDialog) {
			oModel = this.getOwnerComponent().getModel();
			this._saveDialog = sap.ui.xmlfragment('projectum.fragment.saveDialog', this);
		}
		
		return this._saveDialog;	
	},
	

	onSaveButtonPress: function(oEvent) {
		this._saveButton = oEvent.getSource();
		this.getSaveDialog().open();
	},
	
	onSaveDialogConfirm: function() {
		this.saveHours(this._saveButton);
		this.getSaveDialog().close();
	},
	
	onSaveDialogClose: function() {
		this.getSaveDialog().close();
	}
	
});