jQuery.sap.require("projectum.util.Formatter");

sap.ui.controller("projectum.view.projectOverview", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf zyss15t4projectum.app
*/
	onInit: function() {
		
		var oView = this.getView();
		/*
		var sPath = projectum.util.Formatter.sprintf('/Employees(%d)', app.session.getUserId() );
		
		if( app.session.isLoggedIn() ) {
			oView.bindElement( sPath );
		}
		*/
		sap.ui.core.UIComponent.getRouterFor(this).attachRoutePatternMatched(function(oEvent) {
			// when detail navigation occurs, update the binding context
			if ( oEvent.getParameter('name') === 'main' ) {

				var sPath;
				
				if( app.session.isLoggedIn() ) {
					sPath = projectum.util.Formatter.sprintf('/Employees(%d)', app.session.getUserId() );
					oView.bindElement( sPath );
				}

				// Check that the product specified actually was found
				/*
				throws error when not logged in
				oView.getElementBinding().attachEventOnce("dataReceived", jQuery.proxy(function() {
					var oData = oView.getModel().getData(sPath);
					if (!oData) {
						sap.ui.core.UIComponent.getRouterFor(this).myNavToWithoutHash({
							currentView : oView,
							targetViewName : "sap.ui.demo.rm.view.NotFound",
							targetViewType : "XML"
						});
					}
				}, this));
				*/
				
			}
		}, this);
		
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf zyss15t4projectum.app
*/
	onBeforeRendering: function() {
		
	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf zyss15t4projectum.app
*/
	onAfterRendering: function() {
		
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf zyss15t4projectum.app
*/
//	onExit: function() {
//
//	},
	
	onItemPress: function(oEvent) {
		
		var oRouter = sap.ui.core.UIComponent.getRouterFor(this),
			oSource = oEvent.getSource(),
			iProId, iManagerId;
		
		iProId = oSource.getSelectedItem().data('projectId');
		iManagerId = oSource.getSelectedItem().data('managerId');
		
		if(app.session.getUserId() == iManagerId) {
			oRouter.navTo('projectDetailManager', { id : iProId });
		} else {
			oRouter.navTo('projectDetail', { id : iProId });
		}
		
	}
	
	

});