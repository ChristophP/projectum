sap.ui.core.mvc.Controller.extend("projectum.view.app", {

	_userMenu: null,
	
	/**
	 * Called when a controller is instantiated and its View controls (if
	 * available) are already created. Can be used to modify the View before it
	 * is displayed, to bind event handlers and do other one-time
	 * initialization.
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	onInit : function() {
		this.oUpdateFinishedDeferred = jQuery.Deferred();

		/*
		 * this.getView().byId("list").attachEventOnce("updateFinished",
		 * function() { this.oUpdateFinishedDeferred.resolve(); }, this);
		 */
		sap.ui.core.UIComponent.getRouterFor(this).attachRouteMatched(this.onRouteMatched, this);

	},

	/**
	 * Similar to onAfterRendering, but this hook is invoked before the
	 * controller's View is re-rendered (NOT before the first rendering!
	 * onInit() is used for that one!).
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	onBeforeRendering : function() {
		
	},

	/**
	 * Called when the View has been rendered (so its HTML is part of the
	 * document). Post-rendering manipulations of the HTML could be done here.
	 * This hook is the same one that SAPUI5 controls get after being rendered.
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	// onAfterRendering: function() {
	//
	// },
	/**
	 * Called when the Controller is destroyed. Use this one to free resources
	 * and finalize activities.
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	// onExit: function() {
	//
	// }

	onRouteMatched : function(oEvent) {

	},

	onSearch : function() {
		// add filter for search
		var filters = [];
		var searchString = this.getView().byId("searchField").getValue();
		if (searchString && searchString.length > 0) {
			filters = [ new sap.ui.model.Filter("Name",
					sap.ui.model.FilterOperator.Contains, searchString) ];
		}

		// update list binding
		this.getView().byId("list").getBinding("items").filter(filters);
	},

	handleNavButtonPress : function(oEvent) {
		var masterControl = this.getView().getParent();
		masterControl.back();
		sap.ui.core.UIComponent.getRouterFor(this).navTo('main');
	},

	handleUserItemPressed : function(oEvent) {
		
		var oButton = oEvent.getSource();

	    // create menu only once
	    if (!this._userMenu) {
	      this._userMenu = sap.ui.xmlfragment('projectum.fragment.userMenu', this);
	      this.getView().addDependent(this._userMenu);
	    }

	    var eDock = sap.ui.core.Popup.Dock;
	    this._userMenu.open(false, oButton, eDock.CenterTop, eDock.CenterBottom, oButton);
		
	},

	handleLogoffPress : function() {
		var oTabPane = this.getView().byId('tabPane'),
			oIconTabBar = oTabPane.getContent()[0],
			sSelectedKey = oTabPane.createId('tab1');
		
		if(oIconTabBar.getSelectedKey() !== sSelectedKey) {
			oTabPane.getContent()[0].setSelectedKey( sSelectedKey );
		}
		
		
		app.session.logout();
		sap.ui.core.UIComponent.getRouterFor(this).navTo( app.session._deniedRoute );
		
	}

});