sap.ui.controller("projectum.view.login", {

	_dialog: null,
/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf zyss15t4projectum.app
*/
	onInit: function() {
		var oView = sap.ui.getCore().byId('appView'),
			oComp = sap.ui.core.Component.getOwnerComponentFor(oView);
		//oComp.setAggregation('rootControl', this.getView());
		//oComp.render();
		
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf zyss15t4projectum.app
*/
	onBeforeRendering: function() {
		this.getDialog().open();
	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf zyss15t4projectum.app
*/
	onAfterRendering: function() {
		
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf zyss15t4projectum.app
*/
//	onExit: function() {
//
//	},
	
	onSelectionChange: function(oEvent) {
		var oItem = oEvent.getParameters().selectedItem,
			oTabPane = sap.ui.xmlview('projectum.view.tabPane'),
			oUserData;
		
		oUserData = oItem.getBindingContext().getObject();
		
		// store credentials in session
		app.session.login(oUserData);
		
		// change the view
		sap.ui.getCore().byId('appView').addContent( oTabPane );
		
		// redirect to home page
		sap.ui.core.UIComponent.getRouterFor(this).navTo( app.session._grantedRoute );
		
	},
	
	getDialog: function() {
		
		var oModel; 
		
		if(!this._dialog) {
			oModel = this.getOwnerComponent().getModel();
			this._dialog = sap.ui.xmlfragment('projectum.fragment.loginDialog', this);
			this._dialog.setModel(oModel);
		}
		
		return this._dialog;
		
	},
	
	/**
	 * handle selection and redirect accordingly
	 * @param oEvent
	 */
	onConfirm : function(oEvent) {
		var oItem = oEvent.getParameters().selectedItem,
		oUserData;
	
		oUserData = oItem.getBindingContext().getObject();
		
		// store credentials in session
		app.session.login(oUserData);
		
		// redirect to home page
		sap.ui.core.UIComponent.getRouterFor(this).navTo( app.session._grantedRoute );
		
	},
	
	onCancel: function(oEvent) {
		var oItem = oEvent.getParameters().selectedItem,
			self = this;
		
		if(!oItem) {
			sap.m.MessageToast.show('Please select an account', {
				onClose: function() {
					self._dialog.open();
				}
			});
		}
		
	},
	
	handleSearch: function(oEvent) {
        console.log('fire handle search');
	    var sValue = oEvent.getParameter('value').toUpperCase(),
	    	oBinding = oEvent.getSource().getBinding('items');
        // sValue needs to be quoted when toupper is used on the property, seems to be a bug in lib but this work around works
        if(sValue.length) {
            oBinding.filter([
                new sap.ui.model.Filter('toupper(EmpName)', sap.ui.model.FilterOperator.Contains, '\'' + sValue + '\'')
            ]);
        } else {
            oBinding.filter([]);
        }
	  },
	

});