jQuery.sap.require("projectum.util.Formatter");

sap.ui.controller("projectum.view.projectDetailManager", {
	

	_contactDialog: null,
	
	/**
	 * Called when a controller is instantiated and its View
	 * controls (if available) are already created. Can be used
	 * to modify the View before it is displayed, to bind event
	 * handlers and do other one-time initialization.
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	onInit : function() {

		var oView = this.getView();

		// set element bindings for important controls
		sap.ui.core.UIComponent.getRouterFor(this).attachRoutePatternMatched(function(oEvent) {
			// when detail navigation occurs,
			// update the binding context
			if (oEvent.getParameter('name') === 'projectDetailManager') {

				var sProjectPath = projectum.util.Formatter.sprintf('/Projects(%d)', oEvent.getParameter("arguments").id), 
						sEmployeePath = projectum.util.Formatter.sprintf('/Employees(%d)', app.session.getUserId());

				oView.bindElement(sProjectPath);
				
				this.initChart(oEvent.getParameter("arguments").id);

			}
		}, this);

	},

	/**
	 * Similar to onAfterRendering, but this hook is invoked
	 * before the controller's View is re-rendered (NOT before
	 * the first rendering! onInit() is used for that one!).
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	onBeforeRendering : function() {

		// add filtering for list
		// necessary to do it here instead of view because
		// global variable is a filter parameter
		var oView = this.getView();
		oView.byId('taskList').getBinding('items')
				.filter([ new sap.ui.model.Filter("EmpId", "EQ", app.session.getUser().EmpId) ]);
		
		this.initWorktimeList();
		
	},

	/**
	 * Called when the View has been rendered (so its HTML is
	 * part of the document). Post-rendering manipulations of
	 * the HTML could be done here. This hook is the same one
	 * that SAPUI5 controls get after being rendered.
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	onAfterRendering : function() {

	},

	/**
	 * Called when the Controller is destroyed. Use this one to
	 * free resources and finalize activities.
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	// onExit: function() {
	//
	// },

	onIconPress : function() {
		var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		oRouter.navTo('main');
	},

	initChart : function(iProId) {
		var oModel = this.getOwnerComponent().getModel(),
			sPath = '/History', 
			self = this;

		// edit data for presentation
		oModel.read(sPath, {
			//async : false,
			urlParameters : {
				'$expand' : 'Task/Employee,Task/Project'
			},
			success : function(oResponse) {
				// origin point
				var entries = [],
					iWorkloadSum = 0;
				
				oResponse.results.forEach(function(entry, index, arr) {
					var iCurrentWorkload;
					
					if(entry.Task.ProId == iProId && entry.HisStatus == app.timeStatus.APPROVED) {
						iCurrentWorkload = entry.HisWorkload * entry.Task.Employee.EmpHourlyRate;
						iWorkloadSum += iCurrentWorkload;
						entries.push({
							TaskId : entry.TaskId,
							Cost: iWorkloadSum,
							Base: entry.Task.Project.ProBudget / arr.length * index,
							Task: entry.Task
						});
					}
				});
				
				entries.sort(function(a, b) { a.Task.TaskCompletion.getTime() - b.Task.TaskCompletion.getTime(); });
				
				entries.unshift({ TaskId: 0, Cost: 0, Base : 0 });
				
				self._buildChart({ entries: entries });

			},
			error : function() {
				console.log('Courses could not be loaded.');
			}
		});

	},

	/**
	 * build the chart after reading data
	 */
	_buildChart : function(oData) {
		oVizFrame = this.getView().byId("idVizFrameColumn");
		oPopOver = this.getView().byId("idPopOver");
		
		oVizFrame.setModel(new sap.ui.model.json.JSONModel(oData) );

		oVizFrame.removeAllFeeds();
		
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [ {
				name : "TaskId",
				value : "{TaskId}"
			} ],
			measures : [ {
				name : 'Cost',
				value : '{Cost}'
			}, {
				name : 'Baseline',
				value : '{Base}'
			} ],
			data : {
				path : "/entries"
			}
		});

		oVizFrame.setDataset(oDataset);

		var feeds = {
			ValueAxis : new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid' : "valueAxis",
				'type' : "Measure",
				'values' : [ "Cost", "Baseline" ]
			}),
			CategoryAxis : new sap.viz.ui5.controls.common.feeds.FeedItem({
				'uid' : "categoryAxis",
				'type' : "Dimension",
				'values' : [ "TaskId" ]
			})
		};

		oVizFrame.setVizProperties({
			plotArea : {
				dataLabel : {
					visible : true,
					formatString : "#,##0"
				}
			},
			legend : {
				title : {
					visible : false
				}
			},
			title : {
				visible : true,
				text : 'Budget Curve vs. Baseline'
			}
		});

		oVizFrame.addFeed(feeds.ValueAxis);
		oVizFrame.addFeed(feeds.CategoryAxis);
		//oVizFrame.addFeed(feeds.Color);
		oPopOver.connect(oVizFrame.getVizUid());
	},
	
	getContactDialog: function() {
		
		var oModel; 
		
		if(!this._contactDialog) {
			oModel = this.getOwnerComponent().getModel();
			this._contactDialog = sap.ui.xmlfragment('projectum.fragment.contactDialog', this);
			this._contactDialog.setModel(oModel);
		}
		
		return this._contactDialog;
		
	},
	
	onContactPress: function(oEvent) {
		var oDialog = this.getContactDialog(),
			oContext = oEvent.getSource().getBindingContext();
		
		oDialog.bindElement( oContext.getPath() );
		
		oDialog.open();
	},
	
	onContactItemPress: function(oEvent) {
		var oItem = oEvent.getSource().getSelectedItem(),
			sType = oItem.data('contact-type');
		
		if(sType === 'email') {
			sap.m.URLHelper.triggerEmail( oItem.getInfo() );
		} else if(sType === 'phone') {
			sap.m.URLHelper.triggerTel( oItem.getInfo() );
		}
		
	},
	
	onContactDialogButtonPress: function(oEvent) {
		this.getContactDialog().close();
	},
	
	onTaskSelectionChange: function(oEvent) {
		
		var oModel = this.getOwnerComponent().getModel(),
			oItem = oEvent.getParameter('listItem'),
			iTaskId = oItem.data('task-id'),
			sPath = projectum.util.Formatter.sprintf('/Tasks(%d)', iTaskId),
			oPayload = {};
		
		oPayload.TaskCompletion = oItem.getSelected() ?  new Date() : new Date(0);
		
		oModel.update(sPath, oPayload, {
			merge: true,
			success : function(oResponse) {
				sap.m.MessageToast.show('Task completion was successfully changed.');
			},
			error : function() {
				sap.m.MessageToast.show('Task completion is not actually saved to data source in demo mode.');
			}
		});
		
	},
	
	initWorktimeList: function() {
		// Read all history entries omodel.read()
		var oModel = this.getOwnerComponent().getModel(),
			oView = this.getView(),
			sPath = projectum.util.Formatter.sprintf('/History'),
			iProId = projectum.util.Formatter.getIdFromPath( oView.getElementBinding().getPath() ),
			oData = [];
		
		projectum.util.Formatter.getIdFromPath(sPath);
		
		oModel.read(sPath, {
			urlParameters: {
				'$expand': 'Task/Employee'
			},
			success : function(oResponse) {
				console.log('History read.');
				
				oResponse.results.forEach(function(entry) {
					if(entry.HisStatus >= 0 && entry.Task.ProId == iProId) {
						oData.push(entry);
					}
				});
				
				
				oView.setModel(new sap.ui.model.json.JSONModel({ items: oData }), 'history');
				
			},
			error : function() {
				console.log('History could not be read.');
				
			}
		});
		
	},
	
	onStatusIconPress: function(oEvent) {
		var oIcon = oEvent.getSource(), 
			oModel = this.getOwnerComponent().getModel(),
			sPath = projectum.util.Formatter.sprintf('/History(TaskId=%d,HisCw=%d)', oIcon.data('taskId'), 
					oIcon.data('calandarWeek')),
			iStatus = oIcon.data('status'),
			oPayload;
		
		function toggleStatus(iStatus) {
			return iStatus % 2 + 1;
		}

		// save new status in control
		oIcon.data('status', toggleStatus(iStatus));
		
		// status between 1 and 2, approved and rejected
		oPayload = { HisStatus: oIcon.data('status') };

		oModel.update(sPath, oPayload, {
			merge: true,
			success : function(oResponse) {
				var mStatusInfo = app.statusInfo[oPayload.HisStatus];
				oIcon.setColor(mStatusInfo.color)
					.setSrc(mStatusInfo.icon)
					.setTooltip(mStatusInfo.text);
				
				sap.m.MessageToast.show('Work hour status successfully changed to: ' + mStatusInfo.text + '.');
				
			},
			error : function() {
				sap.m.MessageToast.show('Work hour status is not actually saved to data source in demo mode.');
			}
		});
	}

});