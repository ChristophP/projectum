sap.ui.controller("projectum.view.tabPane", {

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf zyss15t4projectum.app
*/
	onInit: function() {

		var oView = this.getView();
		
		// set correct tab, then unbind this handler
		sap.ui.core.UIComponent.getRouterFor(this).attachEventOnce('routePatternMatched', function(oEvent) {
			var oItem = oView.byId('tab2');
			if ( oEvent.getParameter('name') === 'timesheet' ) {
				oItem.getParent().setSelectedItem(oItem);
			}
		}, this);
	},

/**
* Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
* (NOT before the first rendering! onInit() is used for that one!).
* @memberOf zyss15t4projectum.app
*/
	onBeforeRendering: function() {
		
	},

/**
* Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
* This hook is the same one that SAPUI5 controls get after being rendered.
* @memberOf zyss15t4projectum.app
*/
	onAfterRendering: function() {
		
	},

/**
* Called when the Controller is destroyed. Use this one to free resources and finalize activities.
* @memberOf zyss15t4projectum.app
*/
//	onExit: function() {
//
//	},
	
	
	onSelect: function(oEvent) {
		var oRouter = sap.ui.core.UIComponent.getRouterFor(this),
			sId = oEvent.getParameter('item').data('id'),
			sRoute;
			
		switch(sId) {
			case 'tab1':
				sRoute = 'main';
				break;
			case 'tab2':
				sRoute = 'timesheet';
				break;
		}
		oRouter.navTo(sRoute);
	}
	
	
	

});