jQuery.sap.require("projectum.util.Formatter");

sap.ui.controller("projectum.view.projectDetailTeamMember", {

	_contactDialog: null,
	
	/**
	 * Called when a controller is instantiated and its View
	 * controls (if available) are already created. Can be used
	 * to modify the View before it is displayed, to bind event
	 * handlers and do other one-time initialization.
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	onInit : function() {

		var oView = this.getView();

		// set element bindings for important controls
		sap.ui.core.UIComponent.getRouterFor(this).attachRoutePatternMatched(function(oEvent) {
			// when detail navigation occurs,
			// update the binding context
			if (oEvent.getParameter('name') === 'projectDetail') {

				var sProjectPath = projectum.util.Formatter.sprintf('/Projects(%d)',
						oEvent.getParameter("arguments").id);

				oView.bindElement(sProjectPath);
				
				// init Chart
				this.initChart(app.session.getUser().EmpId, oEvent.getParameter("arguments").id);
				
			}
		}, this);

	},

	/**
	 * Similar to onAfterRendering, but this hook is invoked
	 * before the controller's View is re-rendered (NOT before
	 * the first rendering! onInit() is used for that one!).
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	onBeforeRendering : function() {

		// add filtering for list
		// necessary to do it here instead of view because
		// global variable is a filter parameter
		this.getView().byId('taskList').getBinding('items')
				.filter([ new sap.ui.model.Filter("EmpId", "EQ", app.session.getUser().EmpId) ]);
	},

	/**
	 * Called when the View has been rendered (so its HTML is
	 * part of the document). Post-rendering manipulations of
	 * the HTML could be done here. This hook is the same one
	 * that SAPUI5 controls get after being rendered.
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	onAfterRendering : function() {

	},

	/**
	 * Called when the Controller is destroyed. Use this one to
	 * free resources and finalize activities.
	 * 
	 * @memberOf zyss15t4projectum.app
	 */
	// onExit: function() {
	//
	// },

	onIconPress : function() {
		var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
		oRouter.navTo('main');
	},

	initChart : function(empId, ProId) {
		var oModel = this.getOwnerComponent().getModel(),
			oRepos = {
				entries : []
			},
			sPath = projectum.util.Formatter.sprintf('/EmployeesToProjects(EmpId=%d,ProId=%d)/Tasks', empId, ProId), 
			self = this;

		// edit data for presentation
		oModel.read(sPath, {
			//async : false,
			urlParameters : {
				'$expand' : 'History'
			},
			success : function(oResponse) {
				oResponse.results.forEach(function(task) {
					task.History.results.forEach(function(hist) {
						oRepos.entries.push(hist);
					});
				});
				
				self._buildChart(oRepos);

			},
			error : function() {
				console.log('Courses could not be loaded.');
			}
		});

	},

	/**
	 * build the chart after reading data
	 */
	_buildChart : function(oData) {
		oVizFrame = this.getView().byId("idVizFrameColumn");
		oPopOver = this.getView().byId("idPopOver");

		oVizFrame.setModel(new sap.ui.model.json.JSONModel(oData) );

		oVizFrame.removeAllFeeds();
		
		var oDataset = new sap.viz.ui5.data.FlattenedDataset({
			dimensions : [ {
				name : "Calendar Week",
				value : "{HisCw}"
			}, {
				name : "TaskId",
				value : "{TaskId}"
			} ],
			measures : [ {
				name : 'Workhours',
				value : '{HisWorkload}'
			} ],
			data : {
				path : "/entries"
			}
		});

		oVizFrame.setDataset(oDataset);

		var feeds = {
			ValueAxis : new sap.viz.ui5.controls.common.feeds.FeedItem(
					{
						'uid' : "valueAxis",
						'type' : "Measure",
						'values' : [ "Workhours" ]
					}),

			CategoryAxis : new sap.viz.ui5.controls.common.feeds.FeedItem(
					{
						'uid' : "categoryAxis",
						'type' : "Dimension",
						'values' : [ "Calendar Week" ]
					}),

			Color : new sap.viz.ui5.controls.common.feeds.FeedItem(
					{
						'uid' : "color",
						'type' : "Dimension",
						'values' : [ "TaskId" ]
					})

		};

		oVizFrame.setVizProperties({
			plotArea : {
				dataLabel : {
					visible : true,
					formatString : "#,##0"
				}
			},
			legend : {
				title : {
					visible : false
				}
			},
			title : {
				visible : true,
				text : 'Work Hours in the past three weeks'
			}
		});

		oVizFrame.addFeed(feeds.ValueAxis);
		oVizFrame.addFeed(feeds.CategoryAxis);
		oVizFrame.addFeed(feeds.Color);
		oPopOver.connect(oVizFrame.getVizUid());
	},
	
	getContactDialog: function() {
		
		var oModel; 
		
		if(!this._contactDialog) {
			oModel = this.getOwnerComponent().getModel();
			this._contactDialog = sap.ui.xmlfragment('projectum.fragment.contactDialog', this);
			this._contactDialog.setModel(oModel);
		}
		
		return this._contactDialog;
		
	},
	
	onContactPress: function(oEvent) {
		var oDialog = this.getContactDialog(),
			oContext = oEvent.getSource().getBindingContext();
		
		oDialog.bindElement( oContext.getPath() );
		
		oDialog.open();
	},
	
	onContactItemPress: function(oEvent) {
		var oItem = oEvent.getSource().getSelectedItem(),
			sType = oItem.data('contact-type');
		
		if(sType === 'email') {
			sap.m.URLHelper.triggerEmail( oItem.getInfo() );
		} else if(sType === 'phone') {
			sap.m.URLHelper.triggerTel( oItem.getInfo() );
		}
		
	},
	
	onContactDialogButtonPress: function(oEvent) {
		this.getContactDialog().close();
	},
	
	onTaskSelectionChange: function(oEvent) {
		
		var oModel = this.getOwnerComponent().getModel(),
			oItem = oEvent.getParameter('listItem'),
			iTaskId = oItem.data('task-id'),
			sPath = projectum.util.Formatter.sprintf('/Tasks(%d)', iTaskId),
			oPayload = {};
		
		oPayload.TaskCompletion = oItem.getSelected() ?  new Date() : new Date(0);
		
		oModel.update(sPath, oPayload, {
			merge: true,
			success : function(oResponse) {
				sap.m.MessageToast.show('Task completion was successfully changed.');
			},
			error : function() {
				sap.m.MessageToast.show('Task completion could not be changed, likely due to network error.');
			}
		});
		
	}

});