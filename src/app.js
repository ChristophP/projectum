(function(app, $, undefined) {
	app = {
		// Stati for time approval
		timeStatus : {
			PENDING: 0,
			APPROVED: 1,
			REJECTED: 2
		}
		
	};
	
	app.statusInfo = {
			0: {
				icon: 'sap-icon://pending',
				color: '#000000',
				text: 'Pending'
			},
			1: {
				icon: 'sap-icon://accept',
				color: 'green',
				text: 'Approved'
			},
			2: {
				icon: 'sap-icon://sys-cancel-2',
				color: 'red',
				text: 'Rejected'
			}
	};
	
	app.extend = jQuery.extend;
	
	var session = {
			
			_userSessionKey: 'user',
			
			init: function(sGrantedRoute, sDeniedRoute) {
				this._grantedRoute = sGrantedRoute;
				this._deniedRoute = sDeniedRoute;
			},
			
			
			// set user into sessionStorage
			login: function(oUserData) {
				sessionStorage.setItem(this._userSessionKey, JSON.stringify(oUserData) );
				// return app.routing.navToHash( this._appHash );
			},
			
			// delete User from sessionStorage
			logout: function() {
				sessionStorage.removeItem(this._userSessionKey);
				
				// return app.routing.navToHash( this._loginHash );
			},
			
			isLoggedIn: function() {
				return !!( sessionStorage.getItem(this._userSessionKey) );
			},
			
			getUser: function() {
				var sUser = sessionStorage.getItem(this._userSessionKey);
				if(!sUser) return;
				
				return JSON.parse(sUser);
			},
			getUserId: function() {
				var sUser = sessionStorage.getItem(this._userSessionKey);
				if(!sUser) return;
				
				return JSON.parse(sUser).EmpId;
			}
	};
	
	// function to handle authentication
	session.init('main', 'login');
	app.session = session;
	
	
	// export
	window.app = app;

}(window.app = window.app || {}, jQuery));